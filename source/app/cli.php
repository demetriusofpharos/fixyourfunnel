<?php
use \Phalcon\Di\FactoryDefault\Cli as CliDI;
use \Phalcon\Cli\Console as ConsoleApp;


define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

require APP_PATH . '/library/vendor/autoload.php';

// Using the CLI factory
$di = new CliDI();

/**
 * Set variables for the services
 */
include(APP_PATH . '/config/services.php');

/**
 * Include Autoloader
 */
include APP_PATH . '/config/loader.php';

$console = new ConsoleApp();
$console->setDI($di);

/**
 * Process the console arguments
 */
$arguments = [];
foreach ($argv as $k => $arg) {
  if ($k === 1) {
    $arguments["task"] = $arg;

  } elseif ($k === 2) {
    $arguments["action"] = $arg;

  } elseif ($k >= 3) {
    $arguments["params"][] = $arg;
  }
}

try {
  // Handle incoming arguments
  $console->handle($arguments);

} catch (\Phalcon\Exception $e) {
  echo $e->getMessage();
  exit(255);
}

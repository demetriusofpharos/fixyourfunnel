<?php

use \Phalcon\Mvc\Controller;

class BaseController extends Controller {
  protected $acl;
  protected $token;

  /**
   * @inheritDoc
   *
   * runs before all functions
   *
   * - set up ACL from app-wide variables
   * - add fyf.js
   *
   * @return void
   */
  public function initialize() {
    $session  = $this->session->get('fyf-auth');

    $this->acl = \Phalcon\DI::getDefault()->get('useracl');

    $this->token = [
      'user'    => (!empty($session['user_name'])) ? $session['user_name'] : 'anonymous_user',
      'uri'     => (empty($uri)) ? '/' : $uri 
    ];

    /* info */
    $this->assets->addJs('/js/fyf.js');
  }

  /**
   * @inheritDoc
   *
   * checks acl for permissions
   *
   * @return object (Phalcon Request Object | json)
   */
  protected function _securityCheck($session, $permissions, $nested = FALSE, $api = FALSE) {
    /**
     * Secure Access 
     */
    if (!$this->acl->isAllowed($session, $permissions, $nested)) {
      $errorMessage = "Access denied to this page. Contact administrator.";
      Watchdog::accessLog(
        'error',
        $errorMessage,
        ($api == TRUE) ? $this->token['key'] : $this->token['user'],
        $this->token['uri'] 
      );
      
      $response = new \Phalcon\Http\Response();
      $response = $this->_setResponse($response, 403, ['error' => $errorMessage]);
      $response->send();
      exit();
    }
  }

  /**
   * check http method
   *
   * @return object (Phalcon Response Object | json)
   */
  protected function _checkHttpMethod($request, $response, $method = "GET") {
    if ($request->getMethod() != $method) {
      $response = $this->_setResponse($response, 405, ['error' => 'Method Not Allowed'], ['Allow' => $method]);
    }

    return $response;
  }

  /**
   * set response code
   *
   * @return object (Phalcon Response Object | json)
   */
  protected function _setResponse($response, $code, $message, $headers = []) {
    $response->setStatusCode($code);

    foreach ($headers as $h => $c) {
      $response->setHeader($h, $c);
    }

    if (empty($response->getContent())) {
      $response->setContent(json_encode($message));
      $response->setContentType('application/json', 'UTF-8');
    }

    return $response;
  }
}

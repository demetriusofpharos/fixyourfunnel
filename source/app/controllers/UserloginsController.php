<?php 

class UserloginsController extends BaseController {
  /**
   * @inheritDoc
   * 
   * runs before all functions
   * - performs controller wide security check
   * - adds custom JS
   *
   * - views in app/views/userlogins
   */
  public function initialize() {
    parent::initialize();

    $this->_securityCheck($this->session->get('fyf-auth'), ['Users' => [1000 => $this->acl->acl['Users'][1000]]]);

    /* Datatables */
    $this->assets->addJs('/js/datatables.usermanagement.js');
  }

  /**
   * @inheritDoc
   * 
   * default action
   * redirects to listAll
   */
  public function indexAction() {
    $this->response->redirect('/users/listAll');
  }

  /**
   * @inheritDoc
   *
   * list all user logins
   * - sets $users for view
   *
   * @example /userlogins/listAll
   *
   * @return object (Phalcon Request Object)
   */
  public function listAllAction() {
    $users = UserLogins::find([
      'order' => 'created DESC',
    ]);
    
    Watchdog::userLog(
      'info',
      'Viewing all user logins.',
      $this->token['user'],
      $this->token['uri']
    );
    $this->view->setVar('users', $users);
  }
}

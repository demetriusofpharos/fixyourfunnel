<?php 

class UsersController extends BaseController {
  /**
   * @inheritDoc
   *
   * runs before all functions
   * - performs controller wide security check according to whitelist
   * - adds custom CSS
   * - adds custom JS
   *
   * - views in app/views/users
   */
  public function initialize() {
    parent::initialize();

    if (!$this->acl->isAdmin($this->session->get('fyf-auth')) AND !in_array(\Phalcon\DI::getDefault()->getRouter()->getActionName(), ['addUser', 'editUser', 'login', 'logout', 'postUser', 'processLogin'])) {
      $this->_securityCheck($this->session->get('fyf-auth'), ['Users' => [$this->acl->acl['Users']]]);
    }

    /* User CSS */
    $this->assets->addCSS('/css/users.css');

    /* Datatables */
    $this->assets->addJs('/js/datatables.usermanagement.js');
  }

  /**
   * @inheritDoc
   * 
   * default action
   * redirect to listAll
   */
  public function indexAction() {
    $this->response->redirect('/users/listAll');
  }

  /**
   * @inheritDoc
   * 
   * register/add user (UI only)
   * - sets $pass for view
   *
   * @example /users/addUser
   *
   * @return object (Phalcon Request Object)
   */
  public function addUserAction() {
    $sec  = new \Security();
    $pass = $sec->makePassword(rand(16, 25));
    
    $this->view->setVar('pass', $pass);
  }

  /**
   * @inheritDoc
   *
   * edit user (UI only)
   * - sets $user for view
   * - if isAdmin
   *   - sets $acl for view
   *   - sets $roles for view
   *   - sets $perms for view
   *
   * @example /users/editUser
   *
   * @return object (Phalcon Request Object)
   */
  public function editUserAction() {
    $userId = $this->dispatcher->getParams();
    $userId = (!empty($userId)) ? $userId[0] : '' ;

    if ($userId != $this->session->get('fyf-auth')['id']) {
      $this->_securityCheck($this->session->get('fyf-auth'), ['Users' => [20 => $this->acl->acl['Users'][20]]], TRUE);
    }

    $user = '';

    if (!empty($userId)) {
      $user = \Users::getUserById($userId);
    }

    $roles = \UserRoles::find([
      'order' => 'created ASC',
    ]);
    $perms = \UserPermissions::find([
      'order' => 'created ASC',
    ]);

    if ($this->acl->isAdmin($this->session->get('fyf-auth')) AND !empty($user)) {
      $perms = \UserPermissions::getPermissions($user->id);
      $this->view->setVar('acl', $this->acl);
      $this->view->setVar('roles', (!empty($roles)) ? $roles : '' );
      $this->view->setVar('perms', (!empty($perms)) ? $perms : '' );
    }

    $this->view->setVar('user', (!empty($user)) ? $user : '' );
  }

  /**
   * @inheritDoc
   *
   * handle add/edit form
   *
   * @example /users/postUser
   *
   * @return object (Phalcon Request Object)
   */
  public function postUserAction() {
    $this->_checkHttpMethod($this->request, $this->response, 'POST');

    if(empty($this->response->getContent())) {
      $userId = $this->dispatcher->getParams();
      $userId = (!empty($userId)) ? $userId[0] : '' ;
      $my_roles = $this->session->get('fyf-auth');

      /* <Check_Permissions> */
      if (!empty($userId) AND $userId != $my_roles['id']) {
        $this->_securityCheck($my_roles, ['Users' => [20 => $this->acl->acl['Users'][20]]], TRUE);
        if ($this->acl->isManager($my_roles, FALSE)) {
          $users = \Users::find([
            'order' => 'created ASC',
          ]);
          $users = $this->acl->buildUserList($users, $my_roles);

          if (!in_array($userId, array_keys($users))) {
            $this->flashSession->error('Access denied - you don\'t have permission to edit this user.');
            $this->response->redirect('/users/editUser/' . $userId);
            return;
          }
        }
      }
      /* </Check_Permissions> */

      $post                   = $this->request->getPost();
      $sec                    = new \Security();
      $post['user_name']      = trim($post['user_name']);

      /* <Check_Username> */
      $user = \Users::getUserByName($post['user_name']);
      
      if (
        (empty($userId) AND !empty($user)) // Adding a user
        OR
        (!empty($userId) AND !empty($user) AND $user->id != $userId) // Editing a user
      ) {
        $message = 'User ' . $post['user_name'] . ' already exists.';
        \Watchdog::accessLog(
          'info',
          $message,
          $this->token['user'],
          $this->token['uri']
        );

        $this->flashSession->error($message);
        
        if (empty($userId)) {
          $this->response->redirect('/users/addUser');

        } else {
          $this->response->redirect('/users/editUser/' . $userId);
        }
        return;
      }
      /* </Check_Username> */

      /* <Check_Domain> */
      $domain = explode('@', $post['user_name']);
      $valid = (array) $this->internal['validEmails'];
      if (empty($domain[1]) OR !in_array($domain[1], $valid)) {
        if (empty($userId)) {
          $this->flashSession->error('Access denied - you must provide an email from a valid domain.');
          $this->response->redirect('/users/addUser');

        } else {
          $this->flashSession->error('User not saved - you must provide an email from a valid domain.');
          $this->response->redirect('/users/editUser/' . $userId);
        }
        return;
      }
      /* </Check_Domain> */

      if (empty($userId)) {
        $user = new \Users();

      } else {
        $user = \Users::findFirst([
          'conditions' => 'id=?1',
          'bind' => [1 => $userId],
        ]);
      }

      $user->user_name = $post['user_name'];
      if (!empty($post['user_password']) AND $post['user_password'] !== $user->user_password) {
        $post['user_password']  = trim($post['user_password']);
        $post['user_password']  = $sec->makeSecret($post['user_password']);
        $user->user_password    = $post['user_password'];
      }

      if ($user->save()) {
        $message = 'User ' . $post['user_name'] . ' was saved.';
        \Watchdog::accessLog(
          'info',
          $message,
          $this->token['user'],
          $this->token['uri']
        );

        if ($this->acl->isAdmin($this->session->get('fyf-auth')) AND !empty($post['user_role_id'][$user->id])) {
          $perms = \UserPermissions::findFirst([
            'conditions' => 'user_id=?1',
            'bind' => [1 => $user->id],
          ]);
          if (empty($perms)) {
            $perms = new \UserPermissions();
            $perms->user_id = $user->id;
          }
          $perms->user_role_id = json_encode($post['user_role_id'][$user->id]);
          $perms->save();
        
        } else if (empty($userId)) {
          $perms = new \UserPermissions();
          $perms->user_id = $user->id;
          $perms->user_role_id = json_encode(["3" => "on"]);
          $perms->save();
        }

        $this->flashSession->success($message);

      } else {
        $message = 'User ' . $post['user_name'] . ' was not saved.';
        \Watchdog::accessLog(
          'error',
          $message,
          $this->token['user'],
          $this->token['uri']
        );
        $this->flashSession->error($message);
      }
    }

    if ($userId == $this->session->get('fyf-auth')['id']) {
      $this->response->redirect('/users/editUser/' . $userId);
    
    } else {
      $this->response->redirect('/users/listAll');
    }
  }

  /**
   * @inheritDoc
   *
   * marks a given user deleted
   * - overrides security check 
   * - sets $pass for view
   *
   * @example /users/deleteUser/1
   *
   * @return object (Phalcon Request Object)
   */
  public function deleteUserAction() {
    $my_roles = $this->session->get('fyf-auth');
    $this->_securityCheck($my_roles, ['Users' => [30 => $this->acl->acl['Users'][30]]], TRUE);

    $userId = $this->dispatcher->getParams();
    $userId = $userId[0];
    
    $userInfo = Users::findFirst([
      'conditions' => 'id=?1',
      'order' => 'created DESC',
      'bind' => [1 => $userId],
    ]);

    if (!$this->acl->isAdmin($my_roles) AND $userId != $my_roles['id']) {
      if ($this->acl->isManager($my_roles, FALSE)) {
        $userInfo = $this->acl->buildUserList($userInfo, $my_roles);
        if (count($userInfo) > 0) {
          $userInfo = $userInfo[$userId];
        }

      } else {
        $userInfo = [];
      }
    }

    if (!empty($userInfo)) { $userInfo->deleted = time(); }

    if (!empty($userInfo) && $userInfo->save()) {
      $infoMessage = 'The user `' . $userId . '` has been deleted and will no longer be able to log in.';
      Watchdog::userLog(
        'info',
        $infoMessage,
        $this->token['user'],
        $this->token['uri']
      );
      $this->flashSession->notice($infoMessage);

      $perms = UserPermissions::findFirst([
        'conditions' => 'id=?1',
        'bind' => [1 => $userId],
      ]);
      if (!empty($perms)) { $perms->delete(); }

    } else {
      $errorMessage = 'The user `' . $userId . '` was not deleted.';
      Watchdog::userLog(
        'error',
        $errorMessage,
        $this->token['user'],
        $this->token['uri']
      );
      $this->flashSession->error($errorMessage);
    }
    
    $this->response->redirect('/users/listAll');
  }

  /**
   * @inheritDoc
   *
   * list all users
   * - sets $users for view
   *
   * @example /users/listAll
   *
   * @return object (Phalcon Request Object)
   */
  public function listAllAction() {
    $my_roles = $this->session->get('fyf-auth');
    $this->_securityCheck($my_roles, ['Users' => [10 => $this->acl->acl['Users'][10]]], TRUE);

    $users = (object) [];
    if ($this->acl->isManager($my_roles)) {
      $users = \Users::find([
        'order' => 'created ASC',
      ]);

      if (!$this->acl->isAdmin($my_roles)) {
        $users = $this->acl->buildUserList($users, $my_roles);
      }
    }

    Watchdog::userLog(
      'info',
      'Viewing all users.',
      $this->token['user'],
      $this->token['uri']
    );
    $this->view->setVar('users', $users);
  }

  /**
   * @inheritDoc
   *
   * login form (UI only)
   *
   * @example /users/login
   *
   * @return object (Phalcon Request Object)
   */
  public function loginAction() { }

  /**
   * @inheritDoc
   * 
   * logout link (destroys session, redirects)
   *
   * @example /users/logout
   *
   * @return object (Phalcon Request Object)
   */
  public function logoutAction() {
    if (empty($this->session->get('fyf-auth'))) {
      $this->_securityCheck($this->session->get('fyf-auth'), ['Users' => [$this->acl->acl['Users']]]);
    }

    $this->session->remove('fyf-auth');
    $this->response->redirect('/');
  }

  /**
   * @inheritDoc
   *
   * process a login from /users/login
   *
   * @example /users/login
   *
   * @return object (Phalcon Request Object)
   */
  public function processLoginAction() {
    $this->_checkHttpMethod($this->request, $this->response, 'POST');

    if(empty($this->response->getContent())) {
      $post = $this->request->getPost();

      $user = \Users::findFirst([
        'conditions' => 'user_name=?1 AND deleted IS NULL',
        'bind' => [1 => $post['user_name']],
      ]);

      if ($user) {
        $secure = new \Security();
        $auth   = \Users::findFirst([
          'conditions' => 'user_name=?1 AND user_password=?2 AND deleted IS NULL',
          'bind' => [1 => $post['user_name'], 2 => $secure->makeSecret($post['user_password'])]
        ]);

        if ($auth) {
          \Watchdog::userLog(
            'info',
            'Logged in successfully',
            $post['user_name'],
            '/users/login'
          );

          \UserLogins::login(
            $post['user_name'],
            'Logged in successfully'
          );

          $userInfo = [
            'id'        => $user->id,
            'user_name' => $user->user_name,
            'group'     => \UserPermissions::getPermissions($user->id),
            'login'     => time(),
          ];

          $this->registerSession($userInfo);

          $this->response->redirect('/');

        } else {
          $errorMessage = "Login failed: password does not match for user `" . $post['user_name'] . "` or user has been deleted.";
          \Watchdog::userLog(
            'error',
            $errorMessage,
            $post['user_name'],
            '/users/login'
          );

          \UserLogins::login(
            $post['user_name'],
            $errorMessage
          );

          $this->flashSession->error("Error: " . $errorMessage);
          $this->response->redirect('/users/login');
        }

      } else {
        $errorMessage = "Login failed: user `" . $post['user_name'] . "` doesn't exist.";
        \Watchdog::userLog(
          'error',
          $errorMessage,
          $post['user_name'],
          '/users/login'
        );

        \UserLogins::login(
          $post['user_name'],
          $errorMessage
        );

        $this->flashSession->error("Error: " . $errorMessage);
        $this->response->redirect('/users/login');
      }
    }
  }


  /**
   * @inheritDoc
   *
   * register session helper
   * 
   * @param array $userInfo
   *         [ id        => $user->id
   *           user_name => $user->user_name
   *           group     => \UserPermissions::getPermissions($user->id)
   *           login     => time() ]
   *
   * @return object (Phalcon Request Object)
   */
  private function registerSession($userInfo) {
    $this->session->set(
      'fyf-auth',
      $userInfo
    );
  }
}

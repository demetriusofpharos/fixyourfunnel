<?php 
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class PicturesController extends BaseController {
  /**
   * @inheritDoc
   *
   * runs before all functions
   * - performs controller wide security check
   *
   * - views in app/views/images
   */
  public function initialize() {
    parent::initialize();

    /* Search CSS */
    $this->assets->addCSS('/css/search.css');

    /* Datatables */
    $this->assets->addJs('/js/datatables.pictures.js');

    $this->_securityCheck($this->session->get('fyf-auth'), ['ImageUser' => [$this->acl->acl['ImageUser']]]);
  }

  /**
   * @inheritDoc
   * 
   * default action
   * redirects to listAll
   */
  public function indexAction() {
    $this->response->redirect('/pictures/viewAll');
  }

  /**
   * @inheritDoc
   *
   * delete image by ID
   *
   * @example /pictures/deletePicture/1
   *
   * @return object (Phalcon Request Object)
   */
  public function deletePictureAction() {
    $imageId = $this->dispatcher->getParams();
    $user_id = $this->session->get('fyf-auth');

    if (empty($imageId)) {
      $this->flashSession->error("Error: no image found.");

    } else {
      $imageId  = $imageId[0];
      $image    = \Images::getImageById($imageId);

      if (!empty($image)) {
        if ($image->user_id != $user_id['id']) {
          $this->flashSession->error("Error: access denied.");

        } else {
          unlink($image->internal_path);
          unlink($image->thumbnail_path);
          $image->delete();
          $this->flashSession->success("Success: image deleted.");
        }

      } else {
        $this->flashSession->error("Error: no image found.");
      }
    }

    $this->response->redirect('/pictures/manageOwn');
  }

  /**
   * @inheritDoc
   *
   * lists all images
   * - sets $logs for view
   *
   * @example /pictures/viewAll
   *
   * @return object (Phalcon Request Object)
   */
  public function viewAllAction() {
    $images = \Images::find([
      'order' => 'created DESC',
    ]);

    $this->view->setVar('images', $images);
  }

  /**
   * @inheritDoc
   * 
   * uploader for images
   * - set $user_id for view
   *
   * @example /pictures/upload
   *
   * @return object (Phalcon Request Object)
   */
  public function uploadAction() {
    $user_id = $this->session->get('fyf-auth');

    if ($this->request->isPost()) {
      $post   = $this->request->getPost();
      $file   = $this->request->getUploadedFiles();

      if (!empty($file)) { 
        $file   = $file[0];
        $upload = new \FileHelper();

        if($upload->uploadImage($file, $post)) {
          $this->flashSession->success("Success: file uploaded.");
          $query = [
            'conditions' => 'user_id=?1',
            'bind' => [1 => $user_id['id']],
          ];
          $imageId = \Images::findFirstLast('last', 'id', $query);
          $this->response->redirect('/pictures/manage/' . $imageId->id);

        } else {
          $this->flashSession->error("Error: unable to upload file.");
        }
 
      } else {
        $this->flashSession->error("Error: You must upload a file.");
      }

    }
    $this->view->setVar('user_id', $user_id['id']);
  }

  /**
   * @inheritDoc
   * 
   * manage uploaded images by imageId
   *
   * @example /pictures/manage/4
   *
   * @return object (Phalcon Request Object)
   */
  public function manageAction() {
    $imageId = $this->dispatcher->getParams();
    $user_id = $this->session->get('fyf-auth');

    if (empty($imageId)) {
      $this->flashSession->error("Error: no image found.");

    } else {
      $imageId  = $imageId[0];
      $image    = \Images::getImageById($imageId);

      if (!empty($image)) {
        if ($image->user_id != $user_id['id']) {
          $this->flashSession->error("Error: access denied.");
          $this->response->redirect('/pictures/manageOwn');

        } else {
          if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $image->assign($post);
            $image->save();

            $this->flashSession->error("Success: image updated.");
          }
        }

        $this->view->setVar('image', $image);

      } else {
        $this->flashSession->error("Error: no image found.");
        $this->response->redirect('/pictures/viewAll');
      }
    }
  }

  /**
   * @inheritDoc
   * 
   * manage uploaded images by imageId
   *
   * @example /pictures/manage/4
   *
   * @return object (Phalcon Request Object)
   */
  public function manageOwnAction() {
    $user_id = $this->session->get('fyf-auth');
    $images  = \Images::getImagesByUserId($user_id['id']);

    $this->view->setVar('images', $images);    
  }

  /**
   * @inheritDoc
   * 
   * view full size images by imageId
   *
   * @example /pictures/viewFullSize/4
   *
   * @return object (Phalcon Request Object)
   */
  public function viewFullSizeAction() {
    $imageId = $this->dispatcher->getParams();
    $user_id   = $this->session->get('fyf-auth');

    if (empty($imageId)) {
      $this->flashSession->error("Error: no image found.");

    } else {
      $imageId  = $imageId[0];
      $image    = \Images::getImageById($imageId);

      if (!empty($image)) {
        $meta = [];
        list($meta['width'], $meta['height']) = getimagesize($image->internal_path);

        $comments = \ImageComments::getCommentsById($image->id);

        $this->view->setVar('comments', $comments);
        $this->view->setVar('image', $image);
        $this->view->setVar('meta', $meta);
        $this->view->setVar('user_id', $user_id['id']);

      } else {
        $this->flashSession->error("Error: no image found.");
      }
    }
  }

  /**
   * @inheritDoc
   * 
   * save comments
   *
   * @example /pictures/makeComment
   *
   * @return object (Phalcon Request Object)
   */
  public function makeCommentAction() {
    if ($this->request->isPost()) {
      $post = $this->request->getPost();
      $comment = new \ImageComments();
      $comment->assign($post);

      if ($comment->save()) {
        \Watchdog::accessLog(
          'info',
          'Comment saved.',
          $this->token['user'],
          '/pictures/makeComment/' . $post['image_id']
        );
        
        $this->flashSession->success("Success: saved comment.");

      } else {
        \Watchdog::accessLog(
          'error',
          'Comment not saved.',
          $this->token['user'],
          '/pictures/makeComment/' . $post['image_id']
        );

        $this->flashSession->error("Error: no comment could be saved.");
      }

      $this->response->redirect('/pictures/viewFullSize/' . $comment->image_id);

    } else {
      $this->flashSession->error("Error: no comment could be made.");
    }
  }

  /**
   * @inheritDoc
   *
   * delete comment by ID
   *
   * @example /pictures/deleteComment/1
   *
   * @return object (Phalcon Request Object)
   */
  public function deleteCommentAction() {
    $commentId = $this->dispatcher->getParams();
    $user_id   = $this->session->get('fyf-auth');

    if (empty($commentId)) {
      $this->flashSession->error("Error: no comment found.");

    } else {
      $commentId = $commentId[0];
      $comment   = \ImageComments::getCommentById($commentId);

      if (!empty($comment)) {
        $image = \Images::getImageById($comment->image_id);

        if (!in_array($user_id['id'], [$comment->user_id, $image->user_id])) {
          $this->flashSession->error("Error: access denied.");
          $this->response->redirect('/pictures/viewFullSize/' . $comment->image_id);

        } else {
          $comment->delete();
          $this->flashSession->success("Success: comment deleted.");
          $this->response->redirect('/pictures/viewFullSize/' . $comment->image_id);
        }

      } else {
        $this->flashSession->error("Error: no comment found.");
      }
    }

    $this->response->redirect('/pictures/viewAll');
  }
}

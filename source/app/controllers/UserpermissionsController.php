<?php 

class UserpermissionsController extends BaseController {
  /**
   * @inheritDoc
   *
   * runs before all functions
   * - performs controller wide security check
   * - adds custom CSS
   * - adds custom JS
   *
   * views in app/views/userpermissions
   */
  public function initialize() {
    parent::initialize();

    $this->_securityCheck($this->session->get('fyf-auth'), ['Users' => [80 => $this->acl->acl['Users'][80]]], TRUE);

    /* User CSS */
    $this->assets->addCSS('/css/users.css');

    /* Datatables */
    $this->assets->addJs('/js/datatables.usermanagement.js');
  }

  /**
   * @inheritDoc
   * 
   * default action
   * redirects to listAll
   */
  public function indexAction() {
    $this->response->redirect('/userpermissions/listAll');
  }

  /**
   * @inheritDoc
   *
   * handle listAll form
   * - processes listAll, from where you can set permissions for everyone
   *
   * @example /userpermissions/postAllPermissions
   *
   * @return object (Phalcon Request Object)
   */
  public function postAllPermissionsAction() {
    $this->_checkHttpMethod($this->request, $this->response, 'POST');

    if(empty($this->response->getContent())) {
      $my_roles = $this->session->get('ocr-fyf-auth');
      $post   = $this->request->getPost();

      $messages = '';
      $errors   = 0;
      $users = \Users::find([
        'conditions' => 'id NOT IN (1, 4) AND deleted IS NULL',
      ]);
      if (!$this->acl->isAdmin($my_roles)) { 
        if ($this->acl->isManager($my_roles, FALSE)) {
          $users = $this->acl->buildUserList($users, $my_roles);
        }
      }

      foreach ($users as $u) {
        $perm = \UserPermissions::findFirst([
          'conditions'  => 'user_id=?1',
          'bind'        => [1 => $u->id],
        ]);

        if (empty($perm)) {
          $perm = new \UserPermissions();
          $perm->user_id = $u->id;
        }

        if (in_array($u->id, array_keys($post['user_role_id']))) {
          $perm->user_role_id = $post['user_role_id'][$u->id];

        } else {
          $perm->user_role_id = [];
        }

        if ($perm->save()) {
          $messages .= "The permissions for " . $u->user_name . " has been updated.\n ";

        } else {
          $errors++;
          $messages .= "The permissions for " . $u->user_name . " were not updated.\n ";
        }
      }
      
      if ($errors > 0) {
        $this->flashSession->error($messages);

      } else {
        $this->flashSession->success($messages);
      }

      \Watchdog::userLog(
        ($errors > 0) ? 'error' : 'info',
        $messages,
        $this->token['user'],
        $this->token['uri']
      );

    }

    $this->response->redirect('/userpermissions/listAll');
  }

  /**
   * @inheritDoc
   *
   * list all permissions
   * - sets $users for view
   * - sets $roles for view
   * - sets $perms for view
   * - sets $acl for view
   *
   * @example /userpermissions/listAll
   *
   * @return object (Phalcon Request Object)
   */
  public function listAllAction() {
    $my_roles = $this->session->get('ocr-fyf-auth');
    $users = \Users::find([
      'conditions'  => 'id NOT IN (1, 4) AND deleted IS NULL',
      'order'       => 'created ASC',
    ]);

    $roles = \UserRoles::find([
      'order' => 'created ASC',
    ]);

    $perms = \UserPermissions::find([
      'order' => 'created ASC',
    ]);

    if (!$this->acl->isAdmin($my_roles)) { 
      if ($this->acl->isManager($my_roles, FALSE)) {
        $users = $this->acl->buildUserList($users, $my_roles);
        $roles = $this->acl->buildRoleList($roles, $my_roles['group']);
      }
    }
    
    Watchdog::userLog(
      'info',
      'Viewing all user permissions.',
      $this->token['user'],
      $this->token['uri']
    );

    $this->view->setVar('users', $users);
    $this->view->setVar('roles', $roles);
    $this->view->setVar('perms', $perms);
    $this->view->setVar('acl', $this->acl);
  }
}

<?php 

class UserrolesController extends BaseController {
  /**
   * @inheritDoc
   *
   * runs before all functions
   * - performs controller wide security check
   * - adds custom CSS
   * - adds custom JS
   *
   * - views in app/views/userroles
   */
  public function initialize() {
    parent::initialize();

    $this->_securityCheck($this->session->get('fyf-auth'), ['Users' => [40 => $this->acl->acl['Users'][40]]], TRUE);

    /* User CSS */
    $this->assets->addCSS('/css/users.css');

    /* Datatables */
    $this->assets->addJs('/js/datatables.usermanagement.js');
  }

  /**
   * @inheritDoc
   * 
   * default action
   * redirects to listAll
   */
  public function indexAction() {
    $this->response->redirect('/userroles/listAll');
  }

  /**
   * @inheritDoc
   *
   * add role (UI only)
   * - overrides security check
   * - sets $acl for view
   *
   * @example /userroles/addRole
   *
   * @return object (Phalcon Request Object)
   */
  public function addRoleAction() {
    $this->_securityCheck($this->session->get('fyf-auth'), ['Users' => [40 => $this->acl->acl['Users'][40]]], TRUE);

    $this->view->setVar('acl', $this->acl);
  }

  /**
   * @inheritDoc
   *
   * edit role (UI only)
   * - overrides security check
   * - sets $role for view
   * - sets $acl for view
   *
   * @example /userroles/editRole
   *
   * @return object (Phalcon Request Object)
   */
  public function editRoleAction() {
    $this->_securityCheck($this->session->get('fyf-auth'), ['Users' => [50 => $this->acl->acl['Users'][50]]], TRUE);

    $roleId = $this->dispatcher->getParams();
    $roleId = (!empty($roleId)) ? $roleId[0] : '' ;

    if (!empty($roleId)) {
      $role = \UserRoles::getRoleById($roleId);
    }
    
    $this->view->setVar('role', (!empty($role)) ? $role : '' );
    $this->view->setVar('acl', $this->acl);
  }

  /**
   * @inheritDoc
   *
   * handle add/edit form
   *
   * @example /userroles/postRole
   *
   * @return object (Phalcon Request Object)
   */
  public function postRoleAction() {
    $this->_securityCheck($this->session->get('fyf-auth'), ['Users' => [50 => $this->acl->acl['Users'][50]]], TRUE);

    $this->_checkHttpMethod($this->request, $this->response, 'POST');

    if(empty($this->response->getContent())) {
      $post = $this->request->getPost();

      if (empty($post['permissions'])) {
        $post['permissions'] = 'none';

      } else if (is_array($post['permissions'])) {
        $post['permissions'] = json_encode($post['permissions']);
      }

      $role = \UserRoles::findFirst([
        'conditions' => 'role_name=?1',
        'bind' => [1 => $post['role_name']],
      ]);
      if (empty($role)) {
        $role = new \UserRoles();
      }

      $role->assign($post);

      if ($role->save()) {
        $message = 'Role ' . $post['role_name'] . ' was saved.';
        \Watchdog::accessLog(
          'info',
          $message,
          $this->token['user'],
          $this->token['uri']
        );

        $this->flashSession->success($message);

      } else {
        $message = 'Role ' . $post['role_name'] . ' was not saved.';
        \Watchdog::accessLog(
          'error',
          $message,
          $this->token['user'],
          $this->token['uri']
        );
        $this->flashSession->error($message);
      }
    }

    $this->response->redirect('/userroles/listAll');
  }

  /**
   * @inheritDoc
   *
   * handle listAll form
   *
   * @example /userroles/postAllRoles
   *
   * @return object (Phalcon Request Object)
   */
  public function postAllRolesAction() {
    $this->_securityCheck($this->session->get('fyf-auth'), ['Users' => [50 => $this->acl->acl['Users'][50]]], TRUE);

    $this->_checkHttpMethod($this->request, $this->response, 'POST');

    if(empty($this->response->getContent())) {
      $post   = $this->request->getPost();

      $messages = '';
      $errors   = 0;

      if (in_array('delete', array_keys($post))) {
        foreach ($post['delete'] as $id => $val) {
          $r = \UserRoles::findFirst([
            'conditions'  => 'id=?1',
            'bind'        => [1 => $id],
          ]);
          $messages .= "The role " . $r->role_name . " has been deleted.\n ";
          $r->delete();
        }
      }

      $roles  = \UserRoles::find([
                  'conditions'  => 'id NOT IN (1, 2)',
                ]);

      foreach ($roles as $r) {
        $r->permissions = (in_array($r->id, array_keys($post['acl']))) ? json_encode($post['acl'][$r->id]) : 'none';

        if ($r->save()) {
          $messages .= "The role " . $r->role_name . " has been updated.\n ";

        } else {
          $errors++;
          $messages .= "The role " . $r->role_name . " was not updated.\n ";
        }
      }

      if ($errors > 0) {
        $this->flashSession->error($messages);

      } else {
        $this->flashSession->success($messages);
      }

      \Watchdog::userLog(
        ($errors > 0) ? 'error' : 'info',
        $messages,
        $this->token['user'],
        $this->token['uri']
      );
    }

    $this->response->redirect('/userroles/listAll');
  }

  /**
   * @inheritDoc
   *
   * list all roles
   * - sets $roles for view
   * - sets $acl for view
   *
   * @example /userroles/listAll
   *
   * @return object (Phalcon Request Object)
   */
  public function listAllAction() {
    $my_roles = $this->session->get('fyf-auth');
    $this->_securityCheck($this->session->get('fyf-auth'), ['Users' => [40 => $this->acl->acl['Users'][40]]], TRUE);

    $roles = \UserRoles::find([
      'order' => 'created ASC',
    ]);

    if (!$this->acl->isAdmin($my_roles)) { 
      if ($this->acl->isManager($my_roles, FALSE)) {
        $roles = $this->acl->buildRoleList($roles, $my_roles['group']);

      } else {
        $users = [];
      } 
    }
    
    Watchdog::userLog(
      'info',
      'Viewing all user roles.',
      $this->token['user'],
      $this->token['uri']
    );

    $this->view->setVar('roles', $roles);
    $this->view->setVar('acl', $this->acl);
  }
}

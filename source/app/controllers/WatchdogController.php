<?php 
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class WatchdogController extends BaseController {
  /**
   * @inheritDoc
   *
   * runs before all functions
   * - performs controller wide security check
   *
   * - views in app/views/watchdog
   */
  public function initialize() {
    parent::initialize();

    $this->_securityCheck($this->session->get('fyf-auth'), ['Watchdog' => [$this->acl->acl['Watchdog']]]);
  }

  /**
   * @inheritDoc
   * 
   * default action
   * redirects to listAll
   */
  public function indexAction() {
    $this->response->redirect('/watchdog/listAll');
  }

  /**
   * @inheritDoc
   *
   * lists all entries in watchdog
   * - sets $logs for view
   *
   * @example /watchdog/listAll
   *
   * @return object (Phalcon Request Object)
   */
  public function listAllAction() {
    $logs = \Watchdog::find([
      'order' => 'created DESC',
    ]);

    $page = (in_array('page', array_keys($_GET))) ? $_GET['page'] : 0;

    $paginator = new PaginatorModel([
      'data'  => $logs,
      'limit' => 100,
      'page'  => $page,
    ]);

    $this->view->setVar('logs', $paginator->getPaginate());
  }

  /**
   * @inheritDoc
   *
   * view an individual log
   * - sets $log for view
   *
   * @example /watchdog/viewLog/1
   *
   * @return object (Phalcon Request Object)
   */
  public function viewLogAction() {
    $logId = $this->dispatcher->getParams();

    if (!empty($logId)) {
      $logId = $logId[0];
    }

    if (!$logId) {
      $this->flashSession->warning("The 'viewLog' page requires an ID.");
      $this->response->redirect('/watchdog/listAll');

    } else {
      $logInfo = \Watchdog::findFirst([
        'conditions' => 'id=?1',
        'order' => 'created DESC',
        'bind' => [1 => $logId],
      ]);

      $this->view->setVar('log', $logInfo);
    }
  }

  /**
   * @inheritDoc
   *
   * view logs by level
   * - sets $levelId for view
   * - sets $level for view
   * - sets $logs for view
   *
   * @example /watchdog/viewLogLevel/levelId
   *
   * @return object (Phalcon Request Object)
   */
  public function viewLogLevelAction() {
    $levelId = $this->dispatcher->getParams();

    if (!empty($levelId)) {
      $levelId = $levelId[0];
    }

    if (!$levelId) {
      $this->flashSession->notice("The 'viewLogLevel' page requires an ID.");
      $this->response->redirect('/watchdog/listAll');

    } else {
      $logs = \Watchdog::find([
        'conditions' => 'log_level=?1',
        'order' => 'created DESC',
        'bind' => [1 => $levelId],
      ]);

      $page = (in_array('page', array_keys($_GET))) ? $_GET['page'] : 0;

      $paginator = new PaginatorModel([
        'data'  => $logs,
        'limit' => 100,
        'page'  => $page,
      ]);

      $this->view->setVar('levelId', $levelId);
      $this->view->setVar('level', \Watchdog::getLevels()[$levelId]);
      $this->view->setVar('logs', $paginator->getPaginate());
    }
  }

  /**
   * @inheritDoc
   *
   * view logs by type
   * - sets $typeId for view
   * - sets $type for view
   * - sets $logs for view
   *
   * @example /watchdog/viewLogType/typeId
   *
   * @return object (Phalcon Request Object)
   */
  public function viewLogTypeAction() {
    $typeId = $this->dispatcher->getParams();

    if (!empty($typeId)) {
      $typeId = $typeId[0];
    }

    if (!$typeId) {
      $this->flashSession->warning("The 'viewLogType' page requires an ID.");
      $this->response->redirect('/watchdog/listAll');

    } else {
      $logs = \Watchdog::find([
        'conditions' => 'log_type=?1',
        'order' => 'created DESC',
        'bind' => [1 => $typeId]
      ]);

      $page = (in_array('page', array_keys($_GET))) ? $_GET['page'] : 0;

      $paginator = new PaginatorModel([
        'data'  => $logs,
        'limit' => 100,
        'page'  => $page,
      ]);

      $this->view->setVar('typeId', $typeId);
      $this->view->setVar('type', \Watchdog::getTypes()[$typeId]);
      $this->view->setVar('logs', $paginator->getPaginate());
    }
  }

  /**
   * @inheritDoc
   *
   * view report hash log
   * - sets $hashes for view
   *
   * @example /watchdog/viewReportHashes
   *
   * @return object (Phalcon Request Object)
   */
  public function viewReportHashesAction() {
    $hashes = \ReportHash::find([
      'order' => 'created DESC',
    ]);

    $page = (in_array('page', array_keys($_GET))) ? $_GET['page'] : 0;

    $paginator = new PaginatorModel([
      'data'  => $hashes,
      'limit' => 500,
      'page'  => $page,
    ]);

    $this->view->setVar('hashes', $paginator->getPaginate());
  }

  /**
   * @inheritDoc
   *
   * view an individual report hash
   * - sets $hash for view
   *
   * @example /watchdog/viewReportHashLog/1
   *
   * @return object (Phalcon Request Object)
   */
  public function viewReportHashLogAction() {
    $logId = $this->dispatcher->getParams();

    if (!empty($logId)) {
      $logId = $logId[0];
    }

    if (!$logId) {
      $this->flashSession->warning("The 'viewReportHashLog' page requires an ID.");
      $this->response->redirect('/watchdog/listAll');

    } else {
      $logInfo = \ReportHash::findFirst([
        'conditions' => 'id=?1',
        'order' => 'created DESC',
        'bind' => [1 => $logId],
      ]);

      $this->view->setVar('hash', $logInfo);
    }
  }

  /**
   * @inheritDoc
   *
   * view data source log
   *  * without an ID, will display all
   *  * with an ID, will display individual log info
   * - sets $edslog for view
   * - sets $edslogid for view
   *
   * @example /watchdog/viewDataSourceLogs
   * @example /watchdog/viewDataSourceLogs/1
   *
   * @return object (Phalcon Request Object)
   */
  public function viewDataSourceLogsAction() {
    $logId = $this->dispatcher->getParams();

    if (empty($logId)) {
      $edslogs = \EngineDataSourceLog::find([
        'order' => 'created DESC',
      ]);

      $page = (in_array('page', array_keys($_GET))) ? $_GET['page'] : 0;

      $paginator = new PaginatorModel([
        'data'  => $edslogs,
        'limit' => 200,
        'page'  => $page,
      ]);

      $this->view->setVar('edslog', $paginator->getPaginate());
      $this->view->setVar('edslogId', FALSE);

    } else {
      $logId = $logId[0];

      $logInfo = \EngineDataSourceLog::findFirst([
        'conditions' => 'id=?1',
        'order' => 'created DESC',
        'bind' => [1 => $logId],
      ]);

      $this->view->setVar('edslog', $logInfo);
      $this->view->setVar('edslogId', $logId);
    }
  }
}

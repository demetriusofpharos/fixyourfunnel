<?php
use \Phalcon\Config\Adapter\Yaml;
use \Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;

$di->setShared('fyfconfig', function() {
  return new Yaml(
    APP_PATH . '/config/fyf.yml',
    [
      "!base_path" => function($value) {
        return BASE_PATH . $value;
      },
      "!app_path" => function($value) {
        return APP_PATH . $value;
      },
    ]
  );
});

$di->setShared('fyfassess', function() {
  $config = $this->getFyfconfig()->fyfdb;
  return new DbAdapter((array) $config);
});

$di->setShared('internal', function () {
  $config = $this->getFyfconfig();
  return $config->internal;
});

$di->setShared('environment', function () {
  $config = $this->getFyfconfig();
  return $config->environment;
});

$di->setShared('verifySSL', function() {
  $config = $this->getFyfconfig();
  return $config->verifySSL;
});

$di->setShared('useracl', function() {
  $yaml = new Yaml(
    APP_PATH . '/config/acl.yml'
  );
  return new \UserAcl($yaml->toArray());
});

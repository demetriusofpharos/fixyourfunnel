<?php
use \Phalcon\Di\FactoryDefault;
use \Phalcon\Events\Manager as EventsManager;
use \Phalcon\Flash\Session as Flash;
use \Phalcon\Mvc\Application;
use \Phalcon\Mvc\Dispatcher;
use \Phalcon\Mvc\Model\Manager as ModelsManager;
use \Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use \Phalcon\Mvc\View;
use \Phalcon\Mvc\View\Engine\Php as PhpEngine;
use \Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use \Phalcon\Mvc\Url as UrlProvider;
use \Phalcon\Session\Adapter\Files as Session;

//Composer
require(APP_PATH . '/library/vendor/autoload.php');

error_reporting(E_ALL);

try {
  /**
   * The FactoryDefault Dependency Injector automatically registers
   * the services that provide a full stack framework.
   */
  $di = new FactoryDefault();

  /**
   * Set variables for the service
   */
  include(APP_PATH . '/config/services.php');

  /**
   * Handle routes
   */
  include(APP_PATH . '/config/router.php');

  /**
   * Include Autoloader
   */
  include(APP_PATH . '/config/loader.php');
  

  // Setup the view component
  $di->setShared('view', function () {
    $config = $this->getFyfconfig();

    $view = new View();
    $view->setDI($this);
    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines([
      '.volt' => function ($view) {
        $config = $this->getFyfconfig();

        $volt = new VoltEngine($view, $this);

        $volt->setOptions([
          'compiledPath' => '/var/tmp/',
          'compiledSeparator' => '_'
        ]);

        return $volt;
      },
      '.phtml' => PhpEngine::class
    ]);

    return $view;
  });

  // Setup a base URI 
  $di->setShared('url', function () {
    $url = new UrlProvider();
    $url->setBaseUri('/');

    return $url;
  });

  // dispatcher  
  $di->setShared('dispatcher', function () use ($di) {
    $eventsManager = new EventsManager;
    $eventsManager->attach('dispatch:beforeDispatch', new SecureAccess);
    $dispatcher = new Dispatcher;
    $dispatcher->setEventsManager($eventsManager);
    return $dispatcher;
  });

  $di->setShared("flashSession", function () {
    $fs = new Flash([
      "error"   => "alert alert-danger",
      "success" => "alert alert-success text-center",
      "notice"  => "alert alert-info",
      "warning" => "alert alert-warning",
    ]);
    return $fs;
  });

  // Session Service 
  $di->setShared('session', function () {
    $session = new Session();
    $session->start();
    return $session;
  });

  // set up modelsMetadata
  $di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
  });

  // set up modelsManager
  $di->setShared('modelsManager', function () {
    return new ModelsManager();
  });

  /**
   * Handle the request
   */
  $application = new Application($di);

  echo str_replace(["\n","\r","\t"], '', $application->handle()->getContent());

  
} catch (\Exception $e) {
  echo $e->getMessage() . '<br />';
  echo '<pre>' . $e->getTraceAsString() . '</pre>';
}

<?php

$loader = new \Phalcon\Loader();
$config = \Phalcon\DI::getDefault()->get('fyfconfig');

/**
 * Register a set of directories for the auto loader
 */
$loader->registerDirs([
  $config->application->controllersDir,
  $config->application->libraryDir,
  $config->application->modelsDir,
  $config->application->pluginsDir,
])->register();

/**
 * Register a set namespaces
 */
$loader->registerNamespaces([
  
]);

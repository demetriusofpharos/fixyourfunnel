-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: fyfassessdb
-- Generation Time: Mar 16, 2020 at 09:01 PM
-- Server version: 10.3.15-MariaDB-1:10.3.15+maria~bionic
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fyfassess`
--
CREATE DATABASE IF NOT EXISTS `fyfassess` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `fyfassess`;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `internal_path` varchar(255) NOT NULL,
  `thumbnail_path` varchar(255) NOT NULL,
  `ext` varchar(3) NOT NULL,
  `visible_name` varchar(255) NOT NULL,
  `created` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `images`
--

TRUNCATE TABLE `images`;
-- --------------------------------------------------------

--
-- Table structure for table `image_comments`
--

DROP TABLE IF EXISTS `image_comments`;
CREATE TABLE IF NOT EXISTS `image_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `created` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `image_comments`
--

TRUNCATE TABLE `image_comments`;
-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `created` int(10) UNSIGNED NOT NULL,
  `deleted` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `user_password`, `created`, `deleted`) VALUES
(4, 'admin@test.com', 'de20b2449732779bdf57279c0ee8f7020d7cbc72f697f5f24665abf5ae732729', 1567470301, NULL),
(5, 'user1@test.com', 'de20b2449732779bdf57279c0ee8f7020d7cbc72f697f5f24665abf5ae732729', 1584324772, NULL),
(6, 'user2@test.com', 'de20b2449732779bdf57279c0ee8f7020d7cbc72f697f5f24665abf5ae732729', 1584391423, NULL),
(7, 'user3@test.com', 'de20b2449732779bdf57279c0ee8f7020d7cbc72f697f5f24665abf5ae732729', 1584391515, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_logins`
--

DROP TABLE IF EXISTS `user_logins`;
CREATE TABLE IF NOT EXISTS `user_logins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `user_logins`
--

TRUNCATE TABLE `user_logins`;
-- --------------------------------------------------------

--
-- Table structure for table `user_permissions`
--

DROP TABLE IF EXISTS `user_permissions`;
CREATE TABLE IF NOT EXISTS `user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_role_id` varchar(255) NOT NULL,
  `created` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `user_permissions`
--

TRUNCATE TABLE `user_permissions`;
--
-- Dumping data for table `user_permissions`
--

INSERT INTO `user_permissions` (`id`, `user_id`, `user_role_id`, `created`) VALUES
(1, 1, '{\"1\":\"on\"}', 1567470301),
(4, 4, '{\"1\":\"on\"}', 1567470301),
(5, 5, '{\"3\":\"on\"}', 1584324814),
(6, 7, '{\"3\":\"on\"}', 1584391515);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) NOT NULL,
  `permissions` varchar(255) NOT NULL,
  `created` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_name` (`role_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `user_roles`
--

TRUNCATE TABLE `user_roles`;
--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `role_name`, `permissions`, `created`) VALUES
(1, 'Admin', '{\"Admin\":{\"1\":\"on\"}}', 1567470301),
(3, 'Image User', '{\"ImageUser\":{\"10\":\"on\"}}', 1584324733);

-- --------------------------------------------------------

--
-- Table structure for table `watchdog`
--

DROP TABLE IF EXISTS `watchdog`;
CREATE TABLE IF NOT EXISTS `watchdog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log_type` int(3) NOT NULL,
  `log_level` int(2) NOT NULL,
  `log_message` text NOT NULL,
  `log_user` varchar(255) NOT NULL,
  `log_uri` varchar(255) NOT NULL,
  `created` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `watchdog`
--

TRUNCATE TABLE `watchdog`;
--
-- Dumping data for table `watchdog`
--

INSERT INTO `watchdog` (`id`, `log_type`, `log_level`, `log_message`, `log_user`, `log_uri`, `created`) VALUES
(1, 1, 2, 'User mike@test.com was saved.', 'mike@test.com', '/', 1584392359);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

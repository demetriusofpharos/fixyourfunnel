<?php

/**
 * class to allow CLI to read input
 */
class Input {
  public function getInput() {
    $input = fopen('php://stdin', 'r');
    $line  = fgets($input);
    return trim($line);
  }
}

<?php

use Phalcon\Image\Adapter\Imagick as Resize;

/**
 * class to help manage files
 */
class FileHelper {
  public $filter;

  protected $pictures;
  protected $resize;
  protected $security;

  /**
   * make a base FileHelper
   * - set an array of things to filter out of lists
   * - set pictures from YAML
   * - set up a security variable
   *
   * @param array $filter
   *
   * @return void
   */
  public function __construct($filter = '') {
    $this->filter     = $filter;
    $this->pictures   = \Phalcon\Di::getDefault()->getShared('fyfconfig')['internal']['pictures'];
    $this->resize     = \Phalcon\Di::getDefault()->getShared('fyfconfig')['internal']['image_resize'];
    $this->security   = new \Security();
  }

  /**
   * upload a report
   *
   * @param object $phalconFile
   * @param array $arrayInfo
   * @param string $fileName
   *
   * @return boolean
   */
  public function uploadImage($phalconFile, $arrayInfo, $fileName = '') {
    $arrayInfo['created'] = time();
    $arrayInfo['ext']     = $phalconFile->getExtension();
    if (empty($fileName)) {
      $fileName = $this->security->makeUuid('') . '-' . $arrayInfo['created'] ;
    }

    $errors   = 0;
    $pathInfo = $this->__createDirectory($arrayInfo['user_id'], $arrayInfo['created']) . $fileName;

    if ($phalconFile->moveTo($pathInfo . '.' . $arrayInfo['ext'])) {
      $arrayInfo['internal_path']  = $pathInfo . '.' . $arrayInfo['ext'];
      $arrayInfo['thumbnail_path'] = $pathInfo . '_thumb.' . $arrayInfo['ext'];
      $image = new \Images();
      $image->assign($arrayInfo);
      $image->save();

      copy($image->internal_path, $image->thumbnail_path);
      $resize = new Resize($image->thumbnail_path);
      $resize->resize($this->resize->width, $this->resize->height)->save();
      
      \Watchdog::accessLog(
        'info',
        'Uploaded file locally.',
        get_class($this),
        $pathInfo
      );

    } else {
      $errors++;
      \Watchdog::accessLog(
        'error',
        'Unable to upload file locally.',
        get_class($this),
        $pathInfo
      );
    }

    return ($errors > 0) ? FALSE : TRUE;
  }

  /**
   * create a directory on the File System
   *
   * @param string $client
   * @param integer $start
   * @param integer $end
   * @param string $root
   * @param string $type (amazon | downloader)
   *
   * @return string
   */
  public function __createDirectory($user_id, $start, $root = '') {
    if ($root == '') {
      $root = $this->pictures;
    }

    $path  = $root;
    $path .= $user_id . '/';
    $path .= date('Y', $start) . '/';
    $path .= date('m', $start) . '/';

    if (!file_exists($path)) {
      mkdir($path, 0755, true);
    }
    
    return $path;
  }
}

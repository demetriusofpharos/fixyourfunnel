<?php

/**
 * class to manage CLI output and add some color 
 */
class Output {
  protected $colors;

  /**
   * constructor
   * - set color
   *
   * @return void
   */
  public function __construct() {
    $this->colors = new Colors();
  }

  /**
   * wrap a message in color and output it to CLI
   *
   * @param string $msg
   * @param string $fcolor
   * @param string $bcolor
   * @param string $text_postion
   * @param string $effect
   *
   * @return void
   */
  public function cli($msg, $fcolor = 'white', $bcolor = 'black', $text_position = null, $effect = null) {
    $msg = !is_null($text_position) && $text_position == 'left' ? $msg . "\t" : $msg;
    $msg = !is_null($text_position) && $text_position == 'center' ? "\t" . $msg . "\t" : $msg;
    $msg = !is_null($text_position) && $text_position == 'right' ? "\t" . $msg : $msg;

    if (is_array($msg)) {
      $output = '';
      foreach ($msg as $line) {
        $output .= $this->colors->getColorString($line, $fcolor, $bcolor) . "\r\n";
      }
    } else {
      $output = $this->colors->getColorString($msg, $fcolor, $bcolor) . "\r\n";
    }

    /* Effects */
    switch ($effect) {
      case 'typewriter':
        $this->typewriter($output);
        break;
      default:
        echo $output;
        break;
    }
  }

  /**
   * wrapper for the typewriter effect
   *
   * @param string $msg
   *
   * @return void
   */
  public function typewriter($msg) {
    $msg = str_split($msg, 1);
    foreach ($msg as $char) {
      echo $char;
      usleep(2500);
    }
  }

  /**
   * wrapper for the terminal effect
   *
   * @param string $msg
   *
   * @return void
   */
  public function terminal($msg, $color = null) {
    echo "\r\n" . $msg . ' > ';
  }

  /**
   * wrapper for the raw effect
   *
   * @param string $msg
   *
   * @return void
   */
  public function raw($msg) {
    echo $msg . "\r\n";
  }

  /**
   * formatter with print options
   *
   * @param array $opts
   * @param string $fcolor
   * @param string $bcolor
   *
   * @return void
   */
  public function printOptions($opts, $fcolor = null, $bcolor = null) {
    $total_width = exec('tput cols');

    foreach ($opts as $k => $v) {
      echo str_pad($this->colors->getColorString($k . ' . ' . $v, $fcolor, $bcolor), $total_width, ' ', STR_PAD_RIGHT) . "\r\n";
    }
  }

  /**
   * formatter to print header
   *
   * @param string $text
   * @param string $fcolor
   * @param string $bcolor
   *
   * @return void
   */
  public function printHeader($text, $fcolor = null, $bcolor = null) {
    $total_width = exec('tput cols');
    $output      = "\r\n" . str_pad($this->colors->getColorString($text, $fcolor, $bcolor), $total_width, " ", STR_PAD_RIGHT) . "\r\n";
    echo $output;
  }

  /**
   * formatter to print title
   *
   * @param string $text
   * @param string $fcolor
   * @param string $bcolor
   *
   * @return void
   */
  public function printTitle($text, $fcolor = null, $bcolor = null) {
    system('clear');
    $total_width = exec('tput cols');
    $this->cli(str_repeat(' ', $total_width), $fcolor, $bcolor);
    $this->cli(str_repeat(' ', $total_width), $fcolor, $bcolor);
    $this->cli(str_pad($text, $total_width, ' ', STR_PAD_BOTH), $fcolor, $bcolor);
    $this->cli(str_repeat(' ', $total_width), $fcolor, $bcolor);
    $this->cli(str_repeat(' ', $total_width), $fcolor, $bcolor);
  }

  /**
   * console loader
   *
   * @return boolean
   */
  public function loadConsoleTable() {
    return require 'Console/Table.php';
  }

  /**
   * wrap a message in color and output it to CLI
   *
   * @param string $msg
   * @param string $fcolor
   * @param string $bcolor
   * @param string $text_postion
   * @param string $effect
   *
   * @return void
   */
  public function cliOverwrite($msg, $fcolor = 'white', $bcolor = 'black', $text_position = null, $effect = null) {
    $msg = !is_null($text_position) && $text_position == 'left' ? $msg . "\t" : $msg;
    $msg = !is_null($text_position) && $text_position == 'center' ? "\t" . $msg . "\t" : $msg;
    $msg = !is_null($text_position) && $text_position == 'right' ? "\t" . $msg : $msg;

    if (is_array($msg)) {
      $output = '';
      foreach ($msg as $line) {
        $output .= $this->colors->getColorString($line, $fcolor, $bcolor) . "\r";
      }

    } else {
      $output = $this->colors->getColorString($msg, $fcolor, $bcolor) . "\r";
    }

    /* Effects */
    switch ($effect) {
      case 'typewriter':
        $this->typewriter($output);
        break;
      default:
        echo $output;
        break;
    }
  }
}

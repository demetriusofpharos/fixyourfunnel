<?php
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Events\Event;

/**
 * core Access plugin
 * Used in config/fyf.php to perform security check in the 
 *  custom beforeDispatch event set up in app/config/fyf.php
 *
 */
class SecureAccess extends Plugin {
  private $session;
  private $logged_in;

  /**
   * @inheritDoc
   *
   * constructor - set up class vars
   * make sure user is authenticated
   */
  public function __construct() {
    $this->logged_in = FALSE;

    $session = $this->di->get('session');
    if (!empty($session->get('fyf-auth'))) {
      $this->session = $session->get('fyf-auth');

      if (\Phalcon\DI::getDefault()->get('useracl')->isAuthenticated($this->session)) {
        $this->logged_in = TRUE;
      }
    }
  }

  /**
   * @inheritDoc
   *
   * custom event to attach to Phalcons event manager
   * loads custom routes from router and checks their security
   *
   * @param Phalcon\Events\Event $event
   * @param Phalcon\Mvc\Dispatcher $dispatcher
   *
   * @return boolean
   */
  public function beforeDispatch(Event $event, Dispatcher $dispatcher) {
    $controller = $dispatcher->getControllerName();
    $namespace  = $dispatcher->getNamespaceName();
    $action     = $dispatcher->getActionName();

    $api_whitelist = [
    ];

    if (in_array($controller, ['index'])) {
      return;

    } else if ($controller == 'users' AND in_array($action, ['addUser', 'login', 'logout', 'processLogin', 'postUser'])) {
      return TRUE;

    } else if (in_array($namespace, $api_whitelist)) {
      return TRUE;
    }

    /* If not logged in, redirect */
    if (!$this->logged_in) {
      $this->response->redirect('/users/login');
      return FALSE;
    }
  }
}

<?php
use Phalcon\Mvc\User\Plugin;

/**
 * core Acl plugin for users
 */
class UserAcl extends Plugin {
  public $acl = [];

  /**
   * @inheritDoc
   */
  public function __construct($yaml) {
    $this->acl = $yaml;
  }

  /**
   * @inheritDoc
   *
   * Check the custom session array for authentication
   *
   * @param array $session
   *
   * @return boolean
   */
  public function isAuthenticated($session) {
    if (!empty($session)) {
      $user = \Users::findFirst([
        'conditions' => 'user_name=?1 AND deleted IS NULL',
        'bind' => [1 => $session['user_name']],
      ]);

      if ($user) {
        if ($session['login'] > strtotime('-1 week', time())) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * @inheritDoc
   *
   * Check the custom session array for admin group
   *
   * @param array $session
   *
   * @return boolean
   */
  public function isAdmin($session) {
    if (self::isAuthenticated($session) AND
        self::isGroupAssigned($session['group'], 1)
       ) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * @inheritDoc
   *
   * Check the custom session array for manager group
   * Built-in exception for admins
   *
   * @param array $session
   *
   * @return boolean
   */
  public function isManager($session, $adminPass = TRUE) {
    /** 
     * Admins can access everything, and isAuthenticated is built 
     * in to isAdmin, so give admins a pass
     */
    if ($adminPass == TRUE AND self::isAdmin($session)) {
      return TRUE;
    }

    if (self::isAuthenticated($session)) {
      $user_perms = $this->__gatherPerms($session['group']);

      if (in_array('Manager', array_keys($user_perms))) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * @inheritDoc
   *
   * Check user permissions array for the group and id passed
   *
   * @param array $permission
   * @param string $group
   * @param integer $id
   *
   * @return boolean
   */
  public function isPermissionAssigned($permissions, $group, $id) {
    if ($permissions != 'none') {
      if (!is_array($permissions)) { $permissions = json_decode($permissions, TRUE); }

      if (is_array($permissions) AND 
          in_array($group, array_keys($permissions)) AND 
          in_array($id, array_keys($permissions[$group])) AND 
          $permissions[$group][$id] == 'on'
         ) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * @inheritDoc
   *
   * Check the group_perms array for given role_id
   *
   * @param array $group_perms
   * @param integer $role_id
   *
   * @return boolean
   */
  public function isGroupAssigned($group_perms, $role_id) {
    if (!empty($group_perms)) {
      if (!is_array($group_perms)) {
        json_decode($group_perms, TRUE);
      }

      if (!empty($group_perms) AND 
          in_array($role_id, array_keys($group_perms)) AND
          $group_perms[$role_id] == 'on'
      ) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * @inheritDoc
   *
   * Check user permissions against assigned perms
   * The boolean "nested" allows you to pass the object from the ALC Yaml
   *  and the function will check everything in it, not just the top level
   *
   * @param array $session
   * @param array $perms
   * @param boolean $nested
   *
   * @return boolean
   */
  public function isAllowed($session, $perms, $nested = FALSE) {
    /** 
     * Admins can access everything, and isAuthenticated is built 
     * in to isAdmin, so give admins a pass
     */
    if (self::isAdmin($session)) {
      return TRUE;
    }

    /** 
     * If we're checking API permissions, and they're in the API group, 
     * give them a pass
     */
    if (in_array('2', array_keys($session['group'])) AND
        $session['group'][2] == 'on' AND
        in_array('Api', array_keys($perms))
    ) {
      return TRUE;
    }

    /**
     * If we're checking for allowed, the first step is checking
     * if the user is logged in.
     */
    if (self::isAuthenticated($session)) {
      $user_perms = $this->__gatherPerms($session['group']);

      foreach ($perms as $role => $p) {
        if (!in_array($role, array_keys($user_perms))) {
          return FALSE;
        }

        if ($nested == TRUE) {
          foreach ($p as $weight => $name) {
            if (!in_array($weight, array_keys($user_perms[$role])) OR $user_perms[$role][$weight] != 'on') {
              return FALSE;
            }
          }
        }
      }

      return TRUE;
    }

    return FALSE;
  }

  /**
   * @inheritDoc
   *
   * Build a user list that have the same permissions
   *
   * @param object $users (\Users)
   * @param array $allowedPerms
   *
   * @return array filtered_users
   */
  public function buildUserList($users, $allowedPerms) {
    $filtered_users = [];
    
    foreach ($users as $u) {
      $perms = \UserPermissions::getPermissions($u->id);

      if (empty($perms)) {
        $filtered_users[$u->id] = $u;

      } else {
        foreach (array_keys($allowedPerms['group']) as $role_id) {
          if (in_array($role_id, array_keys($perms))) {
            $filtered_users[$u->id] = $u;
          }
        }
      }
    }

    return $filtered_users;
  }

  /**
   * @inheritDoc
   *
   * Build a user list that have the same roles
   *
   * @param object $users (\Users)
   * @param array $allowedRoles
   *
   * @return array filtered_users
   */
  public function buildRoleList($roles, $allowedRoles) {
    $filtered_roles = [];
    
    foreach ($roles as $r) {
      if (in_array($r->id, array_keys($allowedRoles))) {
        $filtered_roles[$r->id] = $r;
      }
    }

    return $filtered_roles;
  }


  /**
   * gather user_perms in a way that we can parse
   *
   * @param array $groups
   *
   * @return array user_perms
   */
  private function __gatherPerms($groups) {
    $user_perms = [];

    foreach ($groups as $gid => $status) {
      if ($status == 'on' && class_exists('UserRoles')) {
        $group = \UserRoles::getRoleById($gid);

        if (!empty($group) AND is_array(json_decode($group->permissions, TRUE))) {
          $user_perms = array_replace_recursive($user_perms, json_decode($group->permissions, TRUE));
        }
      }
    }

    return $user_perms;
  }
}

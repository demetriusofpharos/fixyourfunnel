<?php
use Phalcon\Mvc\User\Plugin;
use Phalcon\Security\Random;

/**
 * core security plugin
 * provides basic secure passwords
 * also allows for encryption/decryption via SSL
 */
class Security extends Plugin {
  protected $cipher = "AES-256-CFB8";

  /**
   * @inheritDoc
   *
   * helper to create a UUID with a prefix
   *
   * @param string $prefix
   *
   * @return string
   */
  public function makeUuid($prefix = 'u_') {
    return uniqid($prefix);
  }

  /**
   * @inheritDoc
   *
   * create a hashed secret from the concatenated string of as many 
   *  args as get passed in
   *
   * @param unlimited
   *
   * @return string (hashed)
   */
  public function makeSecret() {
    $data = '';
    $args = func_get_args();

    if (func_num_args() < 1) {
      $data .= 'I am the very model of a modern major general.';
      $data .= time();

    } else {
      foreach($args as $a) {
        $data .= $a;
      }
    }

    return hash('gost', $data);
  }

  /**
   * @inheritDoc
   *
   * create a password with the number of characters passed in using 
   *  base58 to eliminate the following:
   *    - 0 (zero)
   *    - O (capital o)
   *    - I (capital i)
   *    - l (lower case L)
   *    - + (plus) 
   *    - / (slash)
   *
   * @param integer $num
   *
   * @return string 
   */
  public function makePassword($num = 17) {
    $random = new Random();
    return $random->base58($num);
  }



  /**
   * Pseudo-secure password retrievable encryption functions
   */

  /**
   * @inheritDoc
   *
   * encrypt a string with the provided key and vector (or default vector)
   *
   * @param string $string
   * @param string $key
   * @param string $vector
   *
   * @return string (ssl encrypted)
   */
  public function sslEncrypt($string, $key, $vector = FALSE) {
    $key = $this->makeSslKey($key);
    $vec = ($vector == TRUE) ? $this->makeSslVector() : 'edgebyascential1';

    return openssl_encrypt($string, $this->cipher, $key, 0, $vec);
  }

  /**
   * @inheritDoc
   *
   * decrypt a string with the provided key and vector (or default vector)
   *
   * @param string $string
   * @param string $key
   * @param string $vector
   *
   * @return string (ssl decrypted)
   */
  public function sslDecrypt($string, $key, $vector = FALSE) {
    $key = $this->makeSslKey($key);
    $vec = ($vector == TRUE) ? $this->makeSslVector() : 'edgebyascential1';
    
    return openssl_decrypt($string, $this->cipher, $key, 0, $vec);
  }

  /**
   * @inheritDoc
   * In the case that we need to re-encrypt the password (the key changed
   * but the password didn't), we decrypt and then encrypt the password
   *
   * @param string $string
   * @param string $oldkey
   * @param string $newkey
   * @param string $vector
   *
   * @return string (ssl encrypted)
   */
  public function sslRecrypt($string, $oldkey, $newkey, $vector = FALSE) {
    $string = $this->sslDecrypt($string, $oldkey, $vector);
    $string = $this->sslEncrypt($string, $newkey, $vector);
    return $string;
  }


  /**
   * @inheritDoc
   *
   * get a subset of a string from the concatenated args to use as a key
   *
   * @param unlimited
   *
   * @return string 
   */
  private function makeSslKey() {
    $args = func_get_args();

    $data = self::makeSecret(implode(",", $args));

    return substr($data, 15, 10);
  }

  /**
   * @inheritDoc
   *
   * create a vector to use with SSL
   *
   * @return string 
   */
  private function makeSslVector() {
    $len = openssl_cipher_iv_length($this->cipher);
    $vec = openssl_random_pseudo_bytes($len);

    return $vec;
  }
}

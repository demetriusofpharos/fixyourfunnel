<?php

/**
 * serves as a log table for user logins
 */
class UserLogins extends \BaseModel {  
  /**
   * @Column (type="integer", nullable=false)
   */
  public $user_name;
  
  /**
   * @Column (type="string", length=255, nullable=false)
   */
  public $status;


  /**
   * @inheritDoc
   *
   * expand Model::save for extra processing (most of which has been removed)
   *
   * @param string $username
   * @param string $message
   *
   * @return object
   */
  public static function login($username, $message) {
    $login            = new self();
    $login->user_name = $username;
    $login->status    = $message;
    $login->created   = time();
    $login->save();
    return $login;
  }
}

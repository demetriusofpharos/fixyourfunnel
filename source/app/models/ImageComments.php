<?php

/**
 * stores image info
 */
class ImageComments extends \BaseModel {  
  /**
   * @Column (type="int", length=11, nullable=false)
   */
  public $image_id;

  /**
   * @Column (type="int", length=11, nullable=false)
   */
  public $user_id;

  /**
   * @Column (type="text", nullable=false)
   */
  public $comment;



  /**
   * @inheritDoc
   *
   * retrieve comment from ID
   *
   * @param integer $id
   *
   * @return object (or string on fail)
   */
  public static function getCommentById($id) {
    $comment = self::findFirst([
      'conditions' => 'id=?1',
      'bind' => [1 => $id],
    ]);

    return (!empty($comment)) ? $comment : '' ;
  }

  /**
   * @inheritDoc
   *
   * retrieve comments from specified ID
   *
   * @param integer $id
   *
   * @return object (or string on fail)
   */
  public static function getCommentsById($id, $col = 'image_id') {
    $comments = self::find([
      'conditions' => $col . '=?1',
      'bind' => [1 => $id],
    ]);

    return (!empty($comments)) ? $comments : '' ;
  }
}

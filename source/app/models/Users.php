<?php

/**
 * stores username/password for users
 */
class Users extends \BaseModel {  
  /**
   * @Column (type="string", length=50, nullable=false)
   */
  public $user_name;
  
  /**
   * @Column (type="string", length=255, nullable=false)
   */
  public $user_password;
  
  /**
   * @Column (type="integer", length=10, nullable=true)
   */
  public $deleted;


  /**
   * @inheritDoc
   *
   * retrieve user info from ID
   *
   * @param integer $id
   *
   * @return object (or string on fail)
   */
  public static function getUserById($id) {
    $user = self::findFirst([
      'conditions' => 'id=?1',
      'bind' => [1 => $id],
    ]);

    return (!empty($user)) ? $user : 'No user info available.' ;
  }

  /**
   * @inheritDoc
   *
   * retrieve user info from ID
   *
   * @param integer $id
   *
   * @return object (or string on fail)
   */
  public static function getUserNameById($id) {
    $user = self::getUserById($id);

    return (!empty($user)) ? $user->user_name : 'No user info available.' ;
  }

  /**
   * @inheritDoc
   * 
   * retrieve user info from username
   *
   * @param string $name
   *
   * @return object (or empty on fail)
   */
  public static function getUserByName($name) {
    $user = self::findFirst([
      'conditions' => 'user_name=?1',
      'bind' => [1 => $name],
    ]);

    return (!empty($user)) ? $user : '' ;
  }
}

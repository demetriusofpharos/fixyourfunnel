<?php

/**
 * stores image info
 */
class Images extends \BaseModel {  
  /**
   * @Column (type="int", length=11, nullable=false)
   */
  public $user_id;

  /**
   * @Column (type="string", length=255, nullable=false)
   */
  public $internal_path;

  /**
   * @Column (type="string", length=3, nullable=false)
   */
  public $ext;

  /**
   * @Column (type="string", length=255, nullable=false)
   */
  public $visible_name;


  /**
   * @inheritDoc
   *
   * retrieve image by id
   *
   * @param integer $id
   *
   * @return object (or string on fail)
   */
  public static function getImageById($id) {
    $image = self::findFirst([
      'conditions' => 'id=?1',
      'bind' => [1 => $id],
    ]);

    return (!empty($image)) ? $image : '' ;
  }

  /**
   * @inheritDoc
   *
   * retrieve images from user_id
   *
   * @param integer $id
   *
   * @return object (or string on fail)
   */
  public static function getImagesByUserId($id) {
    $images = self::find([
      'conditions' => 'user_id=?1',
      'bind' => [1 => $id],
    ]);

    return (!empty($images)) ? $images : '' ;
  }
}

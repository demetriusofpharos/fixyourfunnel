<?php

/**
 * stores groups that have been assigned to a user
 */
class UserPermissions extends \BaseModel {  
  /**
   * @Column (type="integer", length=11, nullable=false)
   */
  public $user_id;
  
  /**
   * @Column (type="string", length=255, nullable=false)
   */
  public $user_role_id;


  /**
   * override Model::beforeSave to make sure user_role_id is json
   *
   * @return void
   */
  public function beforeSave() {
    if (gettype($this->user_role_id) !== 'string') {
      $this->user_role_id = json_encode((object)$this->user_role_id);
    }
  }


  /**
   * @inheritDoc
   *
   * retrieve permissions based on $user_id
   *
   * @param integer $user_id (Users::id)
   *
   * @return object (json object) (or empty on fail)
   */
  public static function getPermissions($user_id) {
    $perms = self::findFirst([
      'conditions' => 'user_id=?1',
      'bind' => [1 => $user_id],
    ]);

    return (!empty($perms)) ? json_decode($perms->user_role_id, true) : [] ;
  }
}

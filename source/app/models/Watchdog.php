<?php

/**
 * serves as a log table for the app
 */
class Watchdog extends \BaseModel {
  /**
   * @ Column(type="integer", nullable=false)
   */
  public $log_type;
  
  /**
   * @ Column(type="integer", nullable=false)
   */
  public $log_level;
  
  /**
   * @Column (type="text", nullable=false)
   */
  public $log_message;
  
  /**
   * @Column (type="string", length=255, nullable=false)
   */
  public $log_user;
  
  /**
   * @Column (type="string", length=255, nullable=false)
   */
  public $log_uri;


  /**
   * override Model::beforeSave to make sure log_message is short enough
   *
   * @return void
   */
  public function beforeSave() {
    if (!empty($this->log_message) AND strlen($this->log_message) > 64500) {
      $this->log_message = '...truncated...<br /><br />' . substr($this->log_message, 0, 64500);
    }
  }

  /**
   * @inheritDoc
   * @deprecated in favor of Watchdog::watcher
   */
  public static function accessLog($level, $message, $user, $action) {
    $log = self::watcher('access', $level, $message, $user, $action);
    return $log;
  }

  /**
   * @inheritDoc
   * @deprecated in favor of Watchdog::watcher
   */
  public static function apiLog($level, $message, $user, $action) {
    $log = self::watcher('api', $level, $message, $user, $action);
    return $log;
  }

  /**
   * @inheritDoc
   * @deprecated in favor of Watchdog::watcher
   */
  public static function userLog($level, $message, $user, $action) {
    $log = self::watcher('user', $level, $message, $user, $action);
    return $log;
  }

  /**
   * @inheritDoc
   *
   * overrides Model::save for extra processing (most of which has been removed)
   *
   * @param string $type (Watchdog::getTypes)
   * @param string $level (Watchdog::getLevels)
   * @param string $message
   * @param string $user
   * @param string $action
   *
   * @return object
   */
  public static function watcher($type, $level, $message, $user, $action) {
    $log              = new self();
    $log->log_type    = self::getTypes(TRUE)[$type];
    $log->log_level   = self::getLevels(TRUE)[$level];
    $log->log_message = $message;
    $log->log_user    = $user; //get current user
    $log->log_uri     = $action;
    $log->created     = time();
    $log->save();
    return $log;
  }

  /**
   * @inheritDoc
   * 
   * get types from YAML 
   *
   * @param boolean $keys (flip id=>val)
   *
   * @return array
   */
  public static function getTypes($keys = FALSE) {
    $types = \Phalcon\Di::getDefault()->getShared('internal')['watchdog']['types'];

    if ($keys === TRUE) { $types = array_flip((array) $types); }

    return $types;
  }

  /**
   * @inheritDoc
   * 
   * get levels from YAML 
   *
   * @param boolean $keys (flip id=>val)
   *
   * @return array
   */
  public static function getLevels($keys = FALSE) {
    $levels = \Phalcon\Di::getDefault()->getShared('internal')['watchdog']['levels'];

    if ($keys === TRUE) { $levels = array_flip((array) $levels); }

    return $levels;
  }
}

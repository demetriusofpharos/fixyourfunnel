<?php

/**
 * stores permissions assigned to a group, which can then be assigned to a user
 */
class UserRoles extends \BaseModel {  
  /**
   * @Column (type="string", length=255, nullable=false)
   */
  public $role_name;
  
  /**
   * @Column (type="string", length=255, nullable=false)
   */
  public $permissions;


  /**
   * override Model::beforeSave to make sure permissions is json
   *
   * @return void
   */
  public function beforeSave() {
    if (gettype($this->permissions) !== 'string') {
      $this->permissions = json_encode((object)$this->permissions);
    }
  }


  /**
   * @inheritDoc
   *
   * retrieve role info by id
   *
   * @param integer $id
   *
   * @return object (or string on fail)
   */
  public static function getRoleById($id) {
    $role = self::findFirst([
      'conditions' => 'id=?1',
      'bind' => [1 => $id],
    ]);

    return (!empty($role)) ? $role : "No role info available." ;
  }


  /**
   * @inheritDoc
   * 
   * retrieve role ID by name
   *
   * @param string $name
   *
   * @return integer
   */
  public static function getIdByName($name) {
    $role = self::findFirst([
      'conditions' => 'role_name=?1',
      'bind' => [1 => $name],
    ]);

    return (!empty($role)) ? $role->id : [-1] ;
  }
}

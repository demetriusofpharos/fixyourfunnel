<?php

/**
 * override Phalcon's Model to streamline the app
 */
class BaseModel extends Phalcon\Mvc\Model {
  /**
   * @Primary
   * @Identity
   * @Column (type="integer", nullable=false)
   */
  public $id;
  
  /**
   * @Column (type="integer", length=10, nullable=true)
   */
  public $created;


  /**
   * @inheritDoc
   *
   * default the connection to the appropriate DB
   *
   * @return array
   */
  public function initialize() {
    $this->setConnectionService('fyfassess');
  }

  /**
   * @inheritDoc
   *
   * override the model->save function to log errors
   *
   * @param array $data
   * @param array $whiteList
   *
   * @return boolean
   */
  public function save($data = NULL, $whiteList = NULL) {
    //if (!$this->__isset('id') && !$this->__isset('created')) {
    if (!$this->created) {
      $this->created = time();
    }

    if (!parent::save($data, $whiteList)) {
      if (class_exists('Watchdog')) {
        $error = '';

        foreach ($this->getMessages() as $e) {
          $error .= $e . '<br />';
        }

        \Watchdog::watcher(
          'access',
          'error',
          $error,
          'save_model',
          get_class($this)
        );
      }

      return FALSE;
    }

    return TRUE;
  }

  /**
   * @inheritDoc
   * @deprecated it mostly doesn't get used anyway
   *
   * easy save to mark an entry deleted using timestamp without removing it
   *
   * @param integer $timestamp
   *
   * @return void
   */
  public function mark_deleted($timestamp) {
    $this->deleted = $timestamp;
    $this->save();
  }

  /**
   * @inheritDoc
   *
   * expand Model::findFirst to allow first or last based on col and conditions
   *
   * @param string $which ('first', 'last', or 'both')
   * @param string $col (sort by)
   * @param array $conditions (Phalcon conditions array)
   *
   * @return array
   */
  public static function findFirstLast($which, $col = 'id', $conditions = []) {
    $records  = [];
    //$class    = get_class($this);

    switch ($which) {
      default:
      case 'first':
        $conditions['order'] = $col . ' ASC';
        $records[] = self::findFirst($conditions);
        break;

      case 'last':
        $conditions['order'] = $col . ' DESC';
        $records[] = self::findFirst($conditions);
        break;

      case 'both':
        $conditions['order'] = $col . ' ASC';
        $records[] = self::findFirst($conditions);

        $conditions['order'] = $col . ' DESC';
        $records[] = self::findFirst($conditions);
        break;
    }

    return (!empty($records) && count($records) == 1) ? $records[0] : $records;
  }

  /**
   * @inheritDoc
   *
   * check Model on $col for unique $val
   *
   * @param string $col (column to check)
   * @param string $value (value to check for)
   *
   * @return boolean
   */
  public static function validateUnique($col, $value) {
    $record = self::findFirstLast('first', $col, [
      'conditions'  => $col . '=?1',
      'bind'        => [1 => $value],
    ]);

    return (empty($record)) ? TRUE : FALSE ;
  }
}

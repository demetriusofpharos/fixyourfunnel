$(document).ready(function() {

  $.noConflict();
  $('#user_list').DataTable({
    order: [[2, "asc"]],
    pageLength: 100,
    responsive: true
  });

  $('#user_login_list').DataTable({
    order: [[0, "desc"]],
    pageLength: 100,
    responsive: true
  });

});

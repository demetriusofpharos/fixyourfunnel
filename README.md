#FIX YOUR FUNNEL ASSESSMENT


##Question 1  
```
A SQL database table has grown very large. It has over 100,000,000 entries. The table is written to and read from with some frequency. The table may need to be “sharded” without impacting the use of the software. How would you solve this problem? If there is a better solution, what would that be?
```  
There are questions that need to be answered. Primarily:
1) Is this static data?
2) Is this stale data?

First, a primary/secondary relationship should be set up wherein the primary gets written to and the secondary gets synced to and read from.
If there is static data, consider shunting it off into a NoSQL DB with a good index.
If there is stale data, move it to a secondary table used for history.
If neither is possible, look at normalization.
Failing all of this, split the table (by created date if possible, by primary key if necessary).

If possible, set up a microservice for static reads that syncs the SQL table to a flat file or NoSQL. Possibly with Phalcon, maybe Python depending on complexity.


##Question 2
```
You are assigned to a major project that will attempt to abstract and consolidate similar business logic from 2 different applications. While each application serves a different market and user, each application has some shared functionality. Currently the implementation is written separately, but some functions could be abstracted to a single implementation. How would you determine which functionality should be abstracted and used by both applications. Which tools (languages, cloud services, etc.) would you choose to accomplish your implementation?
```

Again, there are questions that arise.
1) What is the shared functionality? Inventory both apps and make a list.
2) What custom logic surrounds the "shared functionality" - often we think only of CRUD and not more complex math or algorithmic code that affects the save.

Once that is determined, spin up a microservice API (I favor Phalcon over MySQL/MariaDB) that allows both apps (and any future apps) to access the functionality. Docker serves the microservice requirement well.
